// SPDX-License-Identifier: LGPL-2.1-or-later
#include "util.h"

enum {
    TEST_FILE_SIZE = 2 * 1024 * 1024,
};

static char filename[] = "capacity-XXXXXX";

static void cleanup(void)
{
    unlink(filename);
}

int main(int argc, char **argv)
{
    struct test_opts opts;
    struct blkio *b;
    uint64_t capacity;
    int fd;

    parse_generic_opts(&opts, argc, argv);

    ok(blkio_create(opts.driver, &b));
    assert(b);

    err(blkio_get_uint64(b, "capacity", &capacity), -ENODEV);
    err(blkio_set_uint64(b, "capacity", 4096), -EACCES);

    register_cleanup(cleanup);
    fd = create_file(filename, TEST_FILE_SIZE);

    ok(blkio_set_int(b, "fd", fd));
    ok(blkio_connect(b));

    fd = -1; /* ownership passed to libblkio */

    ok(blkio_get_uint64(b, "capacity", &capacity));
    assert(capacity == TEST_FILE_SIZE);

    err(blkio_set_uint64(b, "capacity", 4096), -EACCES);

    ok(blkio_get_uint64(b, "capacity", &capacity));
    assert(capacity == TEST_FILE_SIZE);

    blkio_destroy(&b);
    assert(!b);

    return 0;
}
