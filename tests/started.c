// SPDX-License-Identifier: LGPL-2.1-or-later
#include "util.h"

enum {
    TEST_FILE_SIZE = 2 * 1024 * 1024,
};

static char filename[] = "started-XXXXXX";

static void cleanup(void)
{
    unlink(filename);
}

int main(int argc, char **argv)
{
    struct test_opts opts;
    struct blkio *b;
    int fd;

    parse_generic_opts(&opts, argc, argv);

    ok(blkio_create(opts.driver, &b));
    assert(b);

    register_cleanup(cleanup);
    fd = create_file(filename, TEST_FILE_SIZE);

    ok(blkio_set_int(b, "fd", fd));
    ok(blkio_connect(b));

    /* Calling it twice works */
    ok(blkio_connect(b));

    fd = -1; /* ownership passed to libblkio */

    ok(blkio_start(b));

    /* Calling it twice works */
    ok(blkio_start(b));

    err(blkio_connect(b), -EBUSY);

    blkio_destroy(&b);
    assert(!b);

    return 0;
}
