// SPDX-License-Identifier: LGPL-2.1-or-later
/* Testing utility functions */

#ifndef TESTS_UTIL_H
#define TESTS_UTIL_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <unistd.h>
#include "blkio.h"

/* Check that expr is 0 */
#define ok(expr) \
    do { \
        int ret = (expr); \
        if (ret != 0) { \
            fprintf(stderr, "%s failed (ret %d): %s\n", #expr, ret, \
                    blkio_get_error_msg()); \
            abort(); \
        } \
    } while (0)

/* Check that expr equals expected and that an error message was set. */
#define err(expr, expected) \
    do { \
        int ret = (expr); \
        if (ret != (expected)) { \
            fprintf(stderr, "%s expected return value %d, got %d\n", \
                    #expr, (expected), ret); \
            abort(); \
        } \
        assert(blkio_get_error_msg()[0] != '\0'); \
    } while (0)

/*
 * If condition is true, terminate test with exit code 77, which Meson
 * interprets as skipped.
 */
#define skip_if(condition) \
    do { \
        if (condition) \
            exit(77); \
    } while (0)

struct test_opts {
    char *driver;
};

void parse_generic_opts(struct test_opts *opts, int argc, char **argv);
void register_cleanup(void (*fn)(void));
int create_file(char *namebuf, off_t length);

#endif /* TESTS_UTIL_H */
