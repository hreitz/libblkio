// SPDX-License-Identifier: LGPL-2.1-or-later
#include "util.h"

int main(void)
{
    struct blkio *b;

    err(blkio_create("foo", &b), -ENOENT);
    assert(!b);

    return 0;
}
