// SPDX-License-Identifier: LGPL-2.1-or-later
#include "util.h"

enum {
    TEST_FILE_SIZE = 0,
    TEST_FILE_OFFSET = 0x10000,
};

static char filename[] = "write-XXXXXX";

static void cleanup(void)
{
    unlink(filename);
}

/*
 * Check that "capacity" is properly updated when we query it.
 */
int main(int argc, char **argv)
{
    struct test_opts opts;
    struct blkio *b;
    struct blkioq *q;
    struct blkio_mem_region mem_region;
    struct blkio_completion completion;
    void *buf;     /* I/O buffer */
    size_t buf_size;
    uint64_t capacity;
    int fd;

    parse_generic_opts(&opts, argc, argv);

    ok(blkio_create(opts.driver, &b));
    assert(b);

    register_cleanup(cleanup);
    fd = create_file(filename, TEST_FILE_SIZE);

    ok(blkio_set_int(b, "fd", dup(fd)));
    ok(blkio_connect(b));

    /* Set up I/O buffer */
    buf_size = sysconf(_SC_PAGESIZE);
    ok(blkio_alloc_mem_region(b, &mem_region, buf_size));
    buf = mem_region.addr;

    ok(blkio_set_int(b, "num-queues", 1));
    ok(blkio_start(b));

    ok(blkio_add_mem_region(b, &mem_region));

    q = blkio_get_queue(b, 0);
    assert(q);

    ok(blkio_get_uint64(b, "capacity", &capacity));
    assert(capacity == TEST_FILE_SIZE);

    assert(blkioq_write(q, TEST_FILE_OFFSET, buf, buf_size, NULL, 0) == 0);
    assert(blkioq_submit_and_wait(q, 1, NULL) == 0);
    assert(blkioq_get_completions(q, &completion, 1) == 1);
    assert(completion.ret == buf_size);

    ok(blkio_get_uint64(b, "capacity", &capacity));
    assert(capacity == TEST_FILE_OFFSET + buf_size);

    ok(blkio_del_mem_region(b, &mem_region));
    ok(blkio_free_mem_region(b, &mem_region));

    close(fd);
    blkio_destroy(&b);
    return 0;
}
