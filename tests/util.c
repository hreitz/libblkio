#include <getopt.h>
#include <signal.h>
#include "util.h"

static void (*cleanup)(void);

static void signal_handler(int signo)
{
    cleanup();
}

/* Call fn() at exit or when the process aborts */
void register_cleanup(void (*fn)(void))
{
    struct sigaction sigact = {
        .sa_handler = signal_handler,
        .sa_flags = SA_RESETHAND,
    };

    cleanup = fn;

    sigemptyset(&sigact.sa_mask);
    sigaction(SIGABRT, &sigact, NULL);

    atexit(fn);
}

/* Like mkstemp(3) except it also sets the file size */
int create_file(char *namebuf, off_t length)
{
    int fd = mkstemp(namebuf);
    assert(fd >= 0);

    assert(ftruncate(fd, length) == 0);

    return fd;
}

static const char optstring[] = "d:";
static const struct option longopts[] = {
    {
        .name = "driver",
        .has_arg = required_argument,
        .val = 'd',
    },
    {
        .name = "help",
        .has_arg = no_argument,
        .val = '?',
    },
    {},
};

static void usage(char *exe_name)
{
    fprintf(stderr, "Usage: %s [--help] --driver=<name>\n"
            "\n"
            "Options:\n"
            "  --help                     Print this help message\n"
            "  -d | --driver <name>       Driver name to use in the test\n"
            "",
            exe_name);
    exit(EXIT_FAILURE);
}

void parse_generic_opts(struct test_opts *opts, int argc, char **argv)
{
    opts->driver = NULL;

    for (;;) {
        int opt = getopt_long(argc, argv, optstring, longopts, NULL);

        if (opt == -1)
            break;

        switch (opt) {
        case 'd':
            opts->driver = optarg;
            break;
        case '?':
        default:
            usage(argv[0]);
        }
    }

    if (!opts->driver) {
        usage(argv[0]);
    }
}
