// SPDX-License-Identifier: LGPL-2.1-or-later
#include "util.h"

enum {
    TEST_FILE_SIZE = 2 * 1024 * 1024,
    TEST_NUM_DESCS = 16
};

static char filename[] = "queue-full-XXXXXX";

static void cleanup(void)
{
    unlink(filename);
}

/*
 * Verify that more than num-descs requests can be added.
 */
int main(int argc, char **argv)
{
    struct test_opts opts;
    struct blkio *b;
    struct blkioq *q;
    struct blkio_completion completions[TEST_NUM_DESCS + 2];
    int fd;

    parse_generic_opts(&opts, argc, argv);

    ok(blkio_create(opts.driver, &b));
    assert(b);

    register_cleanup(cleanup);
    fd = create_file(filename, TEST_FILE_SIZE);

    ok(blkio_set_int(b, "fd", fd));
    ok(blkio_connect(b));

    fd = -1; /* ownership passed to libblkio */

    ok(blkio_set_int(b, "num-queues", 1));
    ok(blkio_set_int(b, "num-descs", TEST_NUM_DESCS));
    ok(blkio_start(b));

    q = blkio_get_queue(b, 0);
    assert(q);

    for (int i = 0; i < TEST_NUM_DESCS; i++) {
        assert(blkioq_flush(q, NULL, 0) == 0);
    }

    /* Now exceed num-descs */
    assert(blkioq_flush(q, NULL, 0) == 0);
    assert(blkioq_flush(q, NULL, 0) == 0);

    /* We have to loop in case not all completions can be returned at once */
    for (int remaining = TEST_NUM_DESCS + 2; remaining > 0; ) {
        assert(blkioq_submit_and_wait(q, remaining, NULL) == 0);

        int n = blkioq_get_completions(q, completions, remaining);
        assert(n > 0);
        assert(n <= remaining);
        remaining -= n;
    }

    blkio_destroy(&b);
    return 0;
}
