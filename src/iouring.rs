use crate::blkio::{
    Blkioq, Completion, Driver, Error, MemoryRegion, Queue, ReqFlags, Request, Result, State,
    ValidatedRequest,
};
use crate::properties;
use crate::properties::{PropertiesList, Property};
use io_uring::opcode::{Fallocate, Fsync, Read, Readv, Write, Writev};
use io_uring::types::{Fixed, FsyncFlags};
use libc::{
    c_int, c_void, close, dev_t, eventfd, fcntl, iovec, lseek64, sigset_t, sysconf, EBUSY,
    EFD_CLOEXEC, EFD_NONBLOCK, EINVAL, ENOMEM, ENOTSUP, FALLOC_FL_KEEP_SIZE, FALLOC_FL_PUNCH_HOLE,
    FALLOC_FL_ZERO_RANGE, F_GETFL, O_DIRECT, O_RDWR, O_WRONLY, RWF_DSYNC, SEEK_END, _SC_IOV_MAX,
    _SC_PAGESIZE,
};
use std::cmp;
use std::convert::TryInto;
use std::fs::{self, File, OpenOptions};
use std::io::{self, ErrorKind};
use std::iter;
use std::mem;
use std::num::ParseIntError;
use std::os::linux::fs::MetadataExt;
use std::os::unix::fs::{FileTypeExt, OpenOptionsExt};
use std::os::unix::io::{AsRawFd, FromRawFd, RawFd};
use std::path::{Path, PathBuf};
use std::ptr::null_mut;
use std::str::FromStr;
use std::time::Duration;

// The <linux/io_uring.h> constant is missing from the io_uring crate
const IORING_MAX_ENTRIES: i32 = 32768;

// Hardware queue depth 64-128 is common so use that as the default
const NUM_DESCS_DEFAULT: i32 = 128;

// The io_uring crate exposes a low-level io_uring_enter(2) interface via IoUring.enter() but the
// flag argument constants are private in io_uring::sys. Redefine the value from <linux/io_uring.h>
// here for now.
const IORING_ENTER_GETEVENTS: u32 = 1;

/// Reads a sysfs attribute as an integer
fn sysfs_attr_read<P, T>(path: P) -> io::Result<T>
where
    P: AsRef<Path>,
    T: FromStr<Err = ParseIntError>,
{
    let contents = fs::read(path)?;
    String::from_utf8_lossy(&contents)
        .trim()
        .parse()
        .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))
}

/// Linux block device queue limits
#[derive(Copy, Clone, Debug)]
struct LinuxBlockLimits {
    logical_block_size: u32,
    physical_block_size: u32,
    optimal_io_size: u32,
    write_zeroes_max_bytes: u64,
    discard_alignment: u32,
    discard_alignment_offset: u32,
}

impl LinuxBlockLimits {
    fn from_device_number(device_number: dev_t) -> io::Result<LinuxBlockLimits> {
        let major = unsafe { libc::major(device_number) };
        let minor = unsafe { libc::minor(device_number) };

        // build sysfs paths

        let dev_dir = PathBuf::from(format!("/sys/dev/block/{}:{}", major, minor));

        let is_partition = match dev_dir.join("partition").metadata() {
            Ok(_) => true,
            Err(e) if e.kind() == ErrorKind::NotFound => false,
            Err(e) => return Err(e),
        };

        let queue_dir = dev_dir.join(if is_partition { "../queue" } else { "queue" });

        let dev_attr = |name| sysfs_attr_read(dev_dir.join(name));
        let queue_attr_u32 = |name| -> io::Result<u32> { sysfs_attr_read(queue_dir.join(name)) };
        let queue_attr_u64 = |name| -> io::Result<u64> { sysfs_attr_read(queue_dir.join(name)) };

        // retrieve limits

        // As of Linux 5.17, several block drivers report the discard_alignment queue limit
        // incorrectly, setting it to the discard_granularity instead of 0, which we fix here.
        let discard_alignment = queue_attr_u32("discard_granularity")?;
        let discard_alignment_offset = {
            let value = dev_attr("discard_alignment")?;
            if value == discard_alignment {
                0
            } else {
                value
            }
        };

        // NOTE: When adding more fields to LinuxBlockLimits, either make sure the queried sysfs
        // files exist in all kernel versions where io_uring may be available, or fall back to a
        // default value if they are missing.

        Ok(LinuxBlockLimits {
            logical_block_size: queue_attr_u32("logical_block_size")?,
            physical_block_size: queue_attr_u32("physical_block_size")?,
            optimal_io_size: queue_attr_u32("optimal_io_size")?,
            write_zeroes_max_bytes: queue_attr_u64("write_zeroes_max_bytes")?,
            discard_alignment,
            discard_alignment_offset,
        })
    }
}

/// Information about the block device or regular file that is the target of an [`IoUring`] instance
#[derive(Copy, Clone, Debug)]
struct TargetInfo {
    is_block_device: bool,
    direct: bool,
    read_only: bool,
    request_alignment: i32,
    optimal_io_alignment: i32,
    optimal_io_size: i32,
    supports_write_zeroes_without_fallback: bool,
    discard_alignment: i32,
    discard_alignment_offset: i32,
}

impl TargetInfo {
    fn from_file(file: &File) -> io::Result<TargetInfo> {
        let meta = file.metadata()?;

        let file_status_flags = unsafe { fcntl(file.as_raw_fd(), F_GETFL) };
        if file_status_flags < 0 {
            return Err(io::Error::last_os_error());
        }

        let direct = file_status_flags & O_DIRECT != 0;
        let read_only = file_status_flags & (O_WRONLY | O_RDWR) == 0;

        if meta.file_type().is_block_device() {
            let limits = LinuxBlockLimits::from_device_number(meta.st_rdev())?;

            let request_alignment = if direct {
                limits.logical_block_size as i32
            } else {
                1
            };

            Ok(TargetInfo {
                is_block_device: true,
                direct,
                read_only,
                request_alignment,
                optimal_io_alignment: limits.physical_block_size as i32,
                optimal_io_size: limits.optimal_io_size as i32,
                supports_write_zeroes_without_fallback: limits.write_zeroes_max_bytes > 0,
                discard_alignment: limits.discard_alignment as i32,
                discard_alignment_offset: limits.discard_alignment_offset as i32,
            })
        } else {
            let request_alignment = if direct {
                // This may fail if the file system is not backed by a real block device, so we
                // ignore errors and fall back to the page size, which should always be enough
                // alignment.
                match LinuxBlockLimits::from_device_number(meta.st_dev()) {
                    Ok(limits) => limits.logical_block_size as i32,
                    Err(_) => (unsafe { sysconf(_SC_PAGESIZE) } as i32),
                }
            } else {
                1
            };

            let file_system_block_size = {
                let mut stats: libc::statfs64 = unsafe { mem::zeroed() };
                if unsafe { libc::fstatfs64(file.as_raw_fd(), &mut stats as *mut _) } != 0 {
                    return Err(io::Error::last_os_error());
                }
                stats.f_bsize as i32
            };

            Ok(TargetInfo {
                is_block_device: false,
                direct,
                read_only,
                request_alignment,
                optimal_io_alignment: cmp::max(file_system_block_size, request_alignment),
                optimal_io_size: 0,
                supports_write_zeroes_without_fallback: true,
                // (Correct) file systems don't place alignment restrictions on fallocate(), but
                // property "discard-alignment" must be a multiple of property "request-alignment".
                discard_alignment: request_alignment,
                discard_alignment_offset: 0,
            })
        }
    }
}

struct IoUringQueue {
    target_info: TargetInfo,
    ring: io_uring::IoUring,
    iovecs: Box<[iovec]>,
    iovecs_used: usize,
    supports_read: bool,
    supports_write: bool,
    supports_fallocate: bool,
    eventfd: RawFd,
}

impl IoUringQueue {
    pub fn new(num_descs: u32, fd: RawFd, target_info: &TargetInfo) -> Result<Self> {
        let ring =
            io_uring::IoUring::new(num_descs).map_err(|e| Error::from_io_error(e, ENOMEM))?;

        // io_uring functionality probing was introduced simultaneously with
        // support for (non-vectored) read, write, and fallocate in kernel
        // version 5.6. If the probe fails, we assume that the kernel doesn't
        // support any of these ops. Otherwise, we use it to check for their
        // availability, just in case the kernel in use was patched to include
        // probing but not those ops.

        let mut supports_read = false;
        let mut supports_write = false;
        let mut supports_fallocate = false;

        let mut probe = io_uring::Probe::new();

        if ring.submitter().register_probe(&mut probe).is_ok() {
            supports_read = probe.is_supported(Read::CODE);
            supports_write = probe.is_supported(Write::CODE);
            supports_fallocate = probe.is_supported(Fallocate::CODE);
        }

        ring.submitter()
            .register_files(&[fd])
            .map_err(|e| Error::from_io_error(e, ENOTSUP))?;

        let eventfd = unsafe { eventfd(0, EFD_CLOEXEC | EFD_NONBLOCK) };
        if eventfd < 0 {
            return Err(Error::from_last_os_error());
        }

        let zero_iovec = iovec {
            iov_base: null_mut(),
            iov_len: 0,
        };

        // create IoUringQueue here so eventfd is closed on error
        let queue = IoUringQueue {
            target_info: *target_info,
            ring,
            iovecs: vec![zero_iovec; num_descs.try_into().unwrap()].into_boxed_slice(),
            iovecs_used: 0,
            supports_read,
            supports_write,
            supports_fallocate,
            eventfd,
        };

        queue
            .ring
            .submitter()
            .register_eventfd(eventfd)
            .map_err(|e| Error::from_io_error(e, ENOTSUP))?;

        Ok(queue)
    }

    fn try_readv_for_read(
        &mut self,
        start: u64,
        buf: *mut u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    ) -> bool {
        if self.iovecs_used == self.iovecs.len() {
            return false;
        }

        self.iovecs[self.iovecs_used] = iovec {
            iov_base: buf as *mut c_void,
            iov_len: len,
        };

        let readv_req = ValidatedRequest(Request::Readv {
            start,
            iovec: &self.iovecs[self.iovecs_used] as *const iovec,
            iovcnt: 1,
            user_data,
            flags,
        });

        if !self.try_enqueue(&readv_req) {
            return false;
        }

        self.iovecs_used += 1;
        true
    }

    fn try_writev_for_write(
        &mut self,
        start: u64,
        buf: *const u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    ) -> bool {
        if self.iovecs_used == self.iovecs.len() {
            return false;
        }

        self.iovecs[self.iovecs_used] = iovec {
            iov_base: buf as *mut c_void,
            iov_len: len,
        };

        let writev_req = ValidatedRequest(Request::Writev {
            start,
            iovec: &self.iovecs[self.iovecs_used] as *const iovec,
            iovcnt: 1,
            user_data,
            flags,
        });

        if !self.try_enqueue(&writev_req) {
            return false;
        }

        self.iovecs_used += 1;
        true
    }
}

impl Drop for IoUringQueue {
    fn drop(&mut self) {
        unsafe { close(self.eventfd) };
        self.eventfd = -1;
    }
}

impl Queue for IoUringQueue {
    fn get_completion_fd(&self) -> RawFd {
        self.eventfd
    }

    fn set_completion_fd_enabled(&mut self, _enabled: bool) {
        // TODO: Set/unset IORING_CQ_EVENTFD_DISABLED. The io-uring crate
        // doesn't support this yet.
    }

    fn validate(&self, req: Request) -> Result<ValidatedRequest> {
        match req {
            Request::Read { len, .. } | Request::Write { len, .. } if len > u32::MAX as usize => {
                Err(Error::new(EINVAL, "len must be 32-bit".to_string()))
            }
            Request::WriteZeroes { .. } if !self.supports_fallocate => Err(Error::new(
                ENOTSUP,
                "write_zeroes not supported".to_string(),
            )),
            Request::WriteZeroes {
                start: _,
                len: _,
                user_data: _,
                flags,
            } if self.target_info.is_block_device
                && !self.target_info.supports_write_zeroes_without_fallback
                && flags.contains(ReqFlags::NO_FALLBACK) =>
            {
                Err(Error::new(
                    ENOTSUP,
                    "write_zeroes with BLKIO_REQ_NO_FALLBACK not supported".to_string(),
                ))
            }
            Request::Discard { .. } if !self.supports_fallocate => {
                Err(Error::new(ENOTSUP, "discard not supported".to_string()))
            }
            _ => Ok(ValidatedRequest(req)),
        }
    }

    fn try_enqueue(&mut self, req: &ValidatedRequest) -> bool {
        let entry = match req.0 {
            Request::Read {
                start,
                buf,
                len,
                user_data,
                flags,
            } => {
                if !self.supports_read {
                    return self.try_readv_for_read(start, buf, len, user_data, flags);
                }

                Read::new(Fixed(0), buf, len as u32)
                    .offset(start as i64)
                    .build()
                    .user_data(user_data as u64)
            }
            Request::Write {
                start,
                buf,
                len,
                user_data,
                flags,
            } => {
                if !self.supports_write {
                    return self.try_writev_for_write(start, buf, len, user_data, flags);
                }

                let rw_flags = if flags.contains(ReqFlags::FUA) {
                    RWF_DSYNC
                } else {
                    0
                };

                Write::new(Fixed(0), buf, len as u32)
                    .offset(start as i64)
                    .rw_flags(rw_flags)
                    .build()
                    .user_data(user_data as u64)
            }
            Request::Readv {
                start,
                iovec,
                iovcnt,
                user_data,
                ..
            } => Readv::new(Fixed(0), iovec, iovcnt)
                .offset(start as i64)
                .build()
                .user_data(user_data as u64),
            Request::Writev {
                start,
                iovec,
                iovcnt,
                user_data,
                flags,
            } => {
                let rw_flags = if flags.contains(ReqFlags::FUA) {
                    RWF_DSYNC
                } else {
                    0
                };

                Writev::new(Fixed(0), iovec, iovcnt)
                    .offset(start as i64)
                    .rw_flags(rw_flags)
                    .build()
                    .user_data(user_data as u64)
            }
            Request::Flush { user_data, .. } => Fsync::new(Fixed(0))
                .flags(FsyncFlags::DATASYNC)
                .build()
                .user_data(user_data as u64),
            Request::WriteZeroes {
                start,
                len,
                user_data,
                flags,
            } => {
                #[allow(clippy::collapsible_else_if)]
                let mode = if self.target_info.is_block_device {
                    if self.target_info.supports_write_zeroes_without_fallback {
                        if flags.contains(ReqFlags::NO_UNMAP) {
                            // This will make the kernel call `blkdev_issue_zeroout(...,
                            // BLKDEV_ZERO_NOUNMAP)`, which in this case will simply submit a
                            // REQ_OP_WRITE_ZEROES request with flag REQ_NOUNMAP.
                            FALLOC_FL_ZERO_RANGE | FALLOC_FL_KEEP_SIZE
                        } else {
                            // This will make the kernel call `blkdev_issue_zeroout(...,
                            // BLKDEV_ZERO_NOFALLBACK)`, which in this case will simply submit a
                            // REQ_OP_WRITE_ZEROES request without flag REQ_NOUNMAP.
                            FALLOC_FL_PUNCH_HOLE | FALLOC_FL_KEEP_SIZE
                        }
                    } else {
                        // This will make the kernel call `blkdev_issue_zeroout(...,
                        // BLKDEV_ZERO_NOUNMAP)`, which in this case will emulate a write zeroes request
                        // using regular writes. This is not reached when flags contains
                        // NO_FALLBACK.
                        FALLOC_FL_ZERO_RANGE | FALLOC_FL_KEEP_SIZE
                    }
                } else {
                    if flags.contains(ReqFlags::NO_UNMAP) {
                        FALLOC_FL_ZERO_RANGE
                    } else {
                        // This does not update the file size when requests extend past EOF, but our docs
                        // specify that io_uring write zeroes requests on regular files may or may not
                        // update the file size, so this behavior is correct.
                        FALLOC_FL_PUNCH_HOLE | FALLOC_FL_KEEP_SIZE
                    }
                };

                Fallocate::new(Fixed(0), len as i64)
                    .offset(start as i64)
                    .mode(mode)
                    .build()
                    .user_data(user_data as u64)
            }
            Request::Discard {
                start,
                len,
                user_data,
                ..
            } => {
                const FALLOC_FL_NO_HIDE_STALE: c_int = 0x04;

                Fallocate::new(Fixed(0), len as i64)
                    .offset(start as i64)
                    .mode(FALLOC_FL_PUNCH_HOLE | FALLOC_FL_NO_HIDE_STALE | FALLOC_FL_KEEP_SIZE)
                    .build()
                    .user_data(user_data as u64)
            }
        };

        let result = unsafe { self.ring.submission().push(&entry) };
        result.is_ok()
    }

    fn submit_and_wait(
        &mut self,
        min_completions: u32,
        timeout: Option<Duration>,
        sig: Option<&sigset_t>,
    ) -> Result<()> {
        // TODO implement timeout with Linux 5.11 IORING_ENTER_EXT_ARG in the future
        if timeout.is_some() {
            return Err(Error::new(
                ENOTSUP,
                "io_uring submit_and_wait() timeout not yet implemented",
            ));
        }

        // enter() can hang waiting for completions of backlogged requests if the caller provides a
        // min_completions value that is larger than the sq. Clamp min_completions to the sq size
        // to prevent hangs.
        let sq_capacity: u32 = self.ring.submission().capacity().try_into().unwrap();
        let min_completions = cmp::min(min_completions, sq_capacity);

        let to_submit = self.ring.submission().len() as u32;
        let flags = if min_completions > 0 {
            IORING_ENTER_GETEVENTS
        } else {
            0
        };

        unsafe {
            self.ring
                .submitter()
                .enter(to_submit, min_completions, flags, sig)
                .map_err(|e| Error::from_io_error(e, EINVAL))?;
        }

        self.iovecs_used = 0;

        Ok(())
    }

    fn completions(&mut self) -> Box<dyn Iterator<Item = Completion> + '_> {
        Box::new(self.ring.completion().map(|entry| Completion {
            user_data: entry.user_data() as usize,
            ret: entry.result(),
        }))
    }
}

properties! {
    IOURING_PROPS: PropertyState for IoUring.props {
        fn buf_alignment: i32,
        fn capacity: u64,
        mut direct: bool,
        fn discard_alignment: i32,
        fn discard_alignment_offset: i32,
        driver: str,
        mut fd: i32,
        max_descs: i32,
        fn max_discard_len: u64,
        max_queues: i32,
        max_mem_regions: u64,
        fn max_segments: i32,
        fn max_transfer: i32,
        fn max_write_zeroes_len: u64,
        fn mem_region_alignment: u64,
        needs_mem_regions: bool,
        needs_mem_region_fd: bool,
        mut num_descs: i32,
        mut num_queues: i32,
        fn optimal_io_alignment: i32,
        fn optimal_io_size: i32,
        fn optimal_buf_alignment: i32,
        mut path: str,
        mut read_only: bool,
        fn request_alignment: i32
    }
}

pub struct IoUring {
    props: PropertyState,
    file: Option<File>,
    target_info: Option<TargetInfo>,
    queues: Vec<Blkioq>,
    state: State,
}

impl IoUring {
    pub fn new() -> Self {
        IoUring {
            props: PropertyState {
                direct: false,
                driver: "io_uring".to_string(),
                fd: -1,
                max_descs: IORING_MAX_ENTRIES,
                max_queues: i32::MAX,
                max_mem_regions: u64::MAX,
                needs_mem_regions: false,
                needs_mem_region_fd: false,
                num_descs: NUM_DESCS_DEFAULT,
                num_queues: 1,
                path: String::new(),
                read_only: false,
            },
            file: None,
            target_info: None,
            queues: Vec::new(),
            state: State::Created,
        }
    }

    fn cant_set_while_connected(&self) -> Result<()> {
        if self.state >= State::Connected {
            Err(properties::error_cant_set_while_connected())
        } else {
            Ok(())
        }
    }

    fn cant_set_while_started(&self) -> Result<()> {
        if self.state >= State::Started {
            Err(properties::error_cant_set_while_started())
        } else {
            Ok(())
        }
    }

    fn must_be_connected(&self) -> Result<()> {
        if self.state >= State::Connected {
            Ok(())
        } else {
            Err(properties::error_must_be_connected())
        }
    }

    fn must_be_started(&self) -> Result<()> {
        if self.state >= State::Started {
            Ok(())
        } else {
            Err(Error::new(EBUSY, "Device must be started"))
        }
    }

    fn get_capacity(&self) -> Result<u64> {
        self.must_be_connected()?;

        let capacity = unsafe { lseek64(self.props.fd, 0, SEEK_END) };

        if capacity >= 0 {
            Ok(capacity as u64)
        } else {
            Err(Error::from_last_os_error())
        }
    }

    fn set_direct(&mut self, value: bool) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.direct = value;
        Ok(())
    }

    fn set_fd(&mut self, value: i32) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.fd = value;
        Ok(())
    }

    // Open the file into self.fd
    fn open_file(&mut self) -> Result<()> {
        if !self.props.path.is_empty() {
            if self.props.fd != -1 {
                return Err(Error::new(
                    EINVAL,
                    "path and fd cannot be set at the same time",
                ));
            }

            let open_flags = if self.props.direct { O_DIRECT } else { 0 };

            let file = OpenOptions::new()
                .custom_flags(open_flags)
                .read(true)
                .write(!self.props.read_only)
                .open(self.props.path.as_str())
                .map_err(|e| Error::from_io_error(e, EINVAL))?;

            self.props.fd = file.as_raw_fd();
            self.assign_file(file)
        } else if self.props.fd != -1 {
            let file = unsafe { File::from_raw_fd(self.props.fd) };
            self.assign_file(file)
        } else {
            Err(Error::new(EINVAL, "One of path and fd must be set"))
        }
    }

    fn assign_file(&mut self, file: File) -> Result<()> {
        let file_type = file
            .metadata()
            .map_err(|e| Error::from_io_error(e, EINVAL))?
            .file_type();

        if !file_type.is_block_device() && !file_type.is_file() {
            return Err(Error::new(
                EINVAL,
                "The file must be a block device or a regular file",
            ));
        }

        let target_info =
            TargetInfo::from_file(&file).map_err(|e| Error::from_io_error(e, EINVAL))?;

        // Set the 'direct' and 'read-only' properties to match the file's actual status flags, in
        // case the user specified it through the 'fd' property.
        self.props.direct = target_info.direct;
        self.props.read_only = target_info.read_only;

        self.file = Some(file);
        self.target_info = Some(target_info);

        Ok(())
    }

    fn get_max_segments(&self) -> Result<i32> {
        self.must_be_connected()?;

        // Userspace can submit up to IOV_MAX and the Linux block layer will split requests as
        // needed.
        let ret = unsafe { sysconf(_SC_IOV_MAX) };
        Ok(ret as i32)
    }

    fn get_max_transfer(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(0) // unlimited, Linux block layer will split requests if necessary
    }

    fn get_max_write_zeroes_len(&self) -> Result<u64> {
        self.must_be_connected()?;
        Ok(0) // unlimited, Linux block layer will split requests if necessary
    }

    fn get_max_discard_len(&self) -> Result<u64> {
        self.must_be_connected()?;
        Ok(0) // unlimited, Linux block layer will split requests if necessary
    }

    fn get_mem_region_alignment(&self) -> Result<u64> {
        // no alignment restrictions but must be multiple of buf-alignment
        Ok(self.get_buf_alignment()? as u64)
    }

    fn get_buf_alignment(&self) -> Result<i32> {
        self.must_be_connected()?;
        self.get_request_alignment()
    }

    fn set_num_descs(&mut self, value: i32) -> Result<()> {
        self.must_be_connected()?;
        self.cant_set_while_started()?;

        // TODO check power of two?
        if value <= 0 {
            return Err(Error::new(EINVAL, "num_descs must be greater than 0"));
        }
        if value > IORING_MAX_ENTRIES {
            return Err(Error::new(
                EINVAL,
                format!("num_descs must be smaller than {}", IORING_MAX_ENTRIES),
            ));
        }

        self.props.num_descs = value;
        Ok(())
    }

    fn set_num_queues(&mut self, value: i32) -> Result<()> {
        self.must_be_connected()?;
        self.cant_set_while_started()?;

        if value <= 0 {
            return Err(Error::new(EINVAL, "num_queues must be greater than 0"));
        }

        self.props.num_queues = value;
        Ok(())
    }

    fn get_optimal_io_alignment(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(self.target_info.as_ref().unwrap().optimal_io_alignment)
    }

    fn get_optimal_io_size(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(self.target_info.as_ref().unwrap().optimal_io_size)
    }

    fn get_optimal_buf_alignment(&self) -> Result<i32> {
        self.must_be_connected()?;

        let ret = unsafe { sysconf(_SC_PAGESIZE) };
        Ok(ret as i32)
    }

    fn set_path(&mut self, value: &str) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.path = value.to_string();
        Ok(())
    }

    fn set_read_only(&mut self, value: bool) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.read_only = value;
        Ok(())
    }

    fn get_request_alignment(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(self.target_info.as_ref().unwrap().request_alignment)
    }

    fn get_discard_alignment(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(self.target_info.as_ref().unwrap().discard_alignment)
    }

    fn get_discard_alignment_offset(&self) -> Result<i32> {
        self.must_be_connected()?;
        Ok(self.target_info.as_ref().unwrap().discard_alignment_offset)
    }
}

impl Driver for IoUring {
    fn state(&self) -> State {
        self.state
    }

    fn connect(&mut self) -> Result<()> {
        self.cant_set_while_started()?;

        if self.state == State::Connected {
            return Ok(());
        }

        self.open_file()?;
        self.state = State::Connected;
        Ok(())
    }

    fn start(&mut self) -> Result<()> {
        self.must_be_connected()?;

        if self.state == State::Started {
            return Ok(());
        }

        let target_info = self.target_info.as_ref().unwrap();

        let create_queue = || {
            let q = IoUringQueue::new(self.props.num_descs as u32, self.props.fd, target_info)?;
            Ok(Blkioq::new(Box::new(q)))
        };

        let queues: Result<Vec<_>> = iter::repeat_with(create_queue)
            .take(self.props.num_queues as usize)
            .collect();

        self.queues = queues?;
        self.state = State::Started;

        Ok(())
    }

    // IORING_REGISTER_BUFFERS could be used in the future to improve performance. Ignore
    // memory regions for now.
    fn add_mem_region(&mut self, _region: &MemoryRegion) -> Result<()> {
        self.must_be_started()
    }

    fn del_mem_region(&mut self, _region: &MemoryRegion) -> Result<()> {
        self.must_be_started()
    }

    fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq> {
        self.queues
            .get_mut(index)
            .ok_or_else(|| Error::new(EINVAL, "invalid queue index"))
    }
}
