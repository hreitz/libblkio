use crate::properties::{self, Properties};
use bitflags::bitflags;
use libc::{
    c_void, close, ftruncate, iovec, mmap, munmap, off_t, sigset_t, syscall, SYS_memfd_create,
    EINVAL, EOVERFLOW, MAP_FAILED, MAP_SHARED, MFD_CLOEXEC, PROT_READ, PROT_WRITE,
};
use std::borrow::Cow;
use std::collections::{HashMap, VecDeque};
use std::error;
use std::fmt;
use std::fs::File;
use std::io;
use std::os::unix::io::RawFd;
use std::os::unix::io::{FromRawFd, IntoRawFd};
use std::result;
use std::time::Duration;

// Must be kept in sync with include/blkio.h. Also, when adding a flag, make sure to add a
// corresponding arm in ReqFlags::name().
bitflags! {
    #[repr(transparent)]
    pub struct ReqFlags: u32 {
        const FUA = 1 << 0;
        const NO_UNMAP = 1 << 1;
        const NO_FALLBACK = 1 << 2;
    }
}

impl ReqFlags {
    /// If `self` has a single bit set and it corresponds to a known flag, return its name wrapped
    /// in `Some`. Otherwise, return `None`.
    fn name(self) -> Option<&'static str> {
        match self {
            Self::FUA => Some("FUA"),
            Self::NO_UNMAP => Some("NO_UNMAP"),
            Self::NO_FALLBACK => Some("NO_FALLBACK"),
            _ => None,
        }
    }
}

#[derive(Debug)]
pub struct Error {
    errno: i32,
    message: Cow<'static, str>,
}

impl Error {
    pub fn new<M>(errno: i32, message: M) -> Self
    where
        Cow<'static, str>: From<M>,
    {
        Self {
            errno,
            message: message.into(),
        }
    }

    pub fn from_io_error(io_error: io::Error, default_errno: i32) -> Self {
        Self {
            errno: io_error.raw_os_error().unwrap_or(default_errno),
            message: io_error.to_string().into(),
        }
    }

    pub fn from_last_os_error() -> Self {
        let io_error = io::Error::last_os_error();
        Self {
            errno: io_error.raw_os_error().unwrap(),
            message: io_error.to_string().into(),
        }
    }

    pub fn errno(&self) -> i32 {
        self.errno
    }

    pub fn message(&self) -> &str {
        &self.message
    }
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

pub type Result<T> = result::Result<T, Error>;

pub struct Completion {
    pub user_data: usize,
    pub ret: i32,
}

/// A handy enum for request arguments
pub enum Request {
    Read {
        start: u64,
        buf: *mut u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    },
    Write {
        start: u64,
        buf: *const u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    },
    Readv {
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        flags: ReqFlags,
    },
    Writev {
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        flags: ReqFlags,
    },
    WriteZeroes {
        start: u64,
        len: u64,
        user_data: usize,
        flags: ReqFlags,
    },
    Discard {
        start: u64,
        len: u64,
        user_data: usize,
        flags: ReqFlags,
    },
    Flush {
        user_data: usize,
        flags: ReqFlags,
    },
}

/// A `Request` that has been successfully validated by `Queue::validate()`.
pub struct ValidatedRequest(pub Request);

pub trait Queue {
    fn get_completion_fd(&self) -> RawFd;

    fn set_completion_fd_enabled(&mut self, enabled: bool);

    /// Validate request arguments before enqueuing it. Typically this involves checking flags or
    /// whether the request type is supported and returning EINVAL or ENOTSUP.
    fn validate(&self, req: Request) -> Result<ValidatedRequest>;

    /// Enqueue a request if there is enough space, returning true on success.
    #[must_use]
    fn try_enqueue(&mut self, req: &ValidatedRequest) -> bool;

    fn submit_and_wait(
        &mut self,
        min_completions: u32,
        timeout: Option<Duration>,
        sig: Option<&sigset_t>,
    ) -> Result<()>;

    /// - Iterating over the completions will consume them. An iterator returned
    ///   by a subsequent call to this function will not yield completions that
    ///   have already been consumed.
    ///
    /// - The lower bound returned by [Iterator::size_hint()] initially equals
    ///   the actual number of completions ready to be consumed. Further, it may
    ///   only be decremented as a result of consuming those completions, and
    ///   may or may not be incremented as more completions become ready.
    fn completions(&mut self) -> Box<dyn Iterator<Item = Completion> + '_>;
}

pub fn validate_req_flags(flags: ReqFlags, allowed: ReqFlags) -> Result<()> {
    if allowed.contains(flags) {
        Ok(())
    } else if !ReqFlags::all().contains(flags) {
        Err(Error::new(
            EINVAL,
            format!("unsupported bits in request flags 0x{:08x}", flags),
        ))
    } else {
        let first_disallowed_flag = 1 << (flags & !allowed).bits().trailing_zeros();
        let first_disallowed_flag = ReqFlags::from_bits(first_disallowed_flag).unwrap();
        Err(Error::new(
            EINVAL,
            format!(
                "BLKIO_REQ_{} is invalid for this request type",
                first_disallowed_flag.name().unwrap()
            ),
        ))
    }
}

/// Requests waiting to be enqueued or submitted. Used when `Queue::try_enqueue()` does not have
/// space for a request.
struct Backlog {
    reqs: VecDeque<ValidatedRequest>,
}

impl Backlog {
    fn new() -> Backlog {
        Backlog {
            reqs: VecDeque::new(),
        }
    }

    /// Validate and try to enqueue a request with `try_enqueue()`. If the queue is full, put the
    /// request on the backlog and return success.
    fn enqueue_or_backlog(&mut self, queue: &mut dyn Queue, req: Request) -> Result<()> {
        let req = queue.validate(req)?;

        if !(self.reqs.is_empty() && queue.try_enqueue(&req)) {
            self.reqs.push_back(req);
        }

        Ok(())
    }

    /// Enqueue as many backlogged requests as possible and return the count
    fn process(&mut self, queue: &mut dyn Queue) -> usize {
        let mut count = 0;
        while let Some(req) = self.reqs.pop_front() {
            if !queue.try_enqueue(&req) {
                self.reqs.push_front(req); // reinsert the request into the backlog
                break;
            }
            count += 1;
        }
        count
    }
}

pub struct Blkioq {
    queue: Box<dyn Queue>,
    backlog: Backlog,
}

impl Blkioq {
    pub fn new(queue: Box<dyn Queue>) -> Self {
        Blkioq {
            queue,
            backlog: Backlog::new(),
        }
    }

    pub fn get_completion_fd(&self) -> RawFd {
        self.queue.get_completion_fd()
    }

    pub fn set_completion_fd_enabled(&mut self, enabled: bool) {
        self.queue.set_completion_fd_enabled(enabled);
    }

    /// Enqueue backlogged requests and submit them. Call this after completions have been
    /// processed and there should be space for requests again.
    pub fn enqueue_backlog(&mut self) {
        if self.backlog.process(self.queue.as_mut()) > 0 {
            // TODO propagate error when blkioq_do_io() is introduced
            let _ = self.submit_and_wait(0, None, None);
        }
    }

    pub fn read(
        &mut self,
        start: u64,
        buf: *mut u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    ) -> Result<()> {
        validate_req_flags(flags, ReqFlags::empty())?;

        let req = Request::Read {
            start,
            buf,
            len,
            user_data,
            flags,
        };
        self.backlog.enqueue_or_backlog(&mut *self.queue, req)
    }

    pub fn write(
        &mut self,
        start: u64,
        buf: *const u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    ) -> Result<()> {
        validate_req_flags(flags, ReqFlags::FUA)?;

        let req = Request::Write {
            start,
            buf,
            len,
            user_data,
            flags,
        };
        self.backlog.enqueue_or_backlog(&mut *self.queue, req)
    }

    pub fn readv(
        &mut self,
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        flags: ReqFlags,
    ) -> Result<()> {
        validate_req_flags(flags, ReqFlags::empty())?;

        let req = Request::Readv {
            start,
            iovec,
            iovcnt,
            user_data,
            flags,
        };
        self.backlog.enqueue_or_backlog(&mut *self.queue, req)
    }

    pub fn writev(
        &mut self,
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        flags: ReqFlags,
    ) -> Result<()> {
        validate_req_flags(flags, ReqFlags::FUA)?;

        let req = Request::Writev {
            start,
            iovec,
            iovcnt,
            user_data,
            flags,
        };
        self.backlog.enqueue_or_backlog(&mut *self.queue, req)
    }

    pub fn write_zeroes(
        &mut self,
        start: u64,
        len: u64,
        user_data: usize,
        flags: ReqFlags,
    ) -> Result<()> {
        validate_req_flags(flags, ReqFlags::NO_UNMAP | ReqFlags::NO_FALLBACK)?;

        let req = Request::WriteZeroes {
            start,
            len,
            user_data,
            flags,
        };
        self.backlog.enqueue_or_backlog(&mut *self.queue, req)
    }

    pub fn discard(
        &mut self,
        start: u64,
        len: u64,
        user_data: usize,
        flags: ReqFlags,
    ) -> Result<()> {
        validate_req_flags(flags, ReqFlags::empty())?;

        let req = Request::Discard {
            start,
            len,
            user_data,
            flags,
        };
        self.backlog.enqueue_or_backlog(&mut *self.queue, req)
    }

    pub fn flush(&mut self, user_data: usize, flags: ReqFlags) -> Result<()> {
        validate_req_flags(flags, ReqFlags::empty())?;

        let req = Request::Flush { user_data, flags };
        self.backlog.enqueue_or_backlog(&mut *self.queue, req)
    }

    pub fn submit_and_wait(
        &mut self,
        min_completions: u32,
        timeout: Option<Duration>,
        sig: Option<&sigset_t>,
    ) -> Result<()> {
        self.queue.submit_and_wait(min_completions, timeout, sig)
    }

    /// See [Queue::completions].
    pub fn completions(&mut self) -> Box<dyn Iterator<Item = Completion> + '_> {
        self.queue.completions()
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, PartialOrd)]
pub enum State {
    Created,   // after blkio_new()
    Connected, // after blkio_connect()
    Started,   // after blkio_start()
}

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct MemoryRegion {
    pub addr: usize,
    pub iova: u64,
    pub len: usize,
    pub fd: RawFd,
    pub fd_offset: i64,
    pub flags: u32,
}

pub trait Driver: Properties {
    fn state(&self) -> State;
    fn connect(&mut self) -> Result<()>;
    fn start(&mut self) -> Result<()>;

    /// The allocated region is _not_ added (registered) by this method.
    fn alloc_mem_region(&mut self, len: usize) -> Result<MemoryRegion> {
        if self.state() < State::Connected {
            return Err(properties::error_must_be_connected());
        }

        let align = self.get_u64("mem-region-alignment")? as usize;

        if len % align != 0 {
            return Err(Error::new(
                EINVAL,
                format!("len {} violates mem-region-alignment {}", len, align),
            ));
        }

        let fd =
            unsafe { syscall(SYS_memfd_create, "libblkio-buf\0".as_ptr(), MFD_CLOEXEC) } as i32;
        if fd < 0 {
            return Err(Error::from_io_error(
                std::io::Error::last_os_error(),
                EINVAL,
            ));
        }

        // Automatically close the fd in error code paths
        let file = unsafe { File::from_raw_fd(fd) };

        if unsafe { ftruncate(fd, len as off_t) } < 0 {
            return Err(Error::from_io_error(
                std::io::Error::last_os_error(),
                EINVAL,
            ));
        }

        let addr = unsafe {
            mmap(
                std::ptr::null_mut(),
                len,
                PROT_READ | PROT_WRITE,
                MAP_SHARED,
                fd,
                0,
            )
        };
        if addr == MAP_FAILED {
            return Err(Error::from_io_error(
                std::io::Error::last_os_error(),
                EINVAL,
            ));
        }

        // Give up if the address is unaligned. Don't attempt to align manually because
        // "mem-region-alignment" should not exceed the page size in practice.
        if (addr as usize) % align != 0 {
            unsafe { munmap(addr, len) };
            return Err(Error::new(
                EOVERFLOW,
                format!(
                    "Address {} violates mem-region-alignment {}",
                    addr as usize, align,
                ),
            ));
        }

        Ok(MemoryRegion {
            addr: addr as usize,
            iova: 0,
            len,
            fd: file.into_raw_fd(),
            fd_offset: 0,
            flags: 0,
        })
    }

    /// The given region must _not_ be registered when this method is called.
    fn free_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        if self.state() < State::Connected {
            return Err(properties::error_must_be_connected());
        }

        let ret = unsafe { munmap(region.addr as *mut c_void, region.len) };
        if ret < 0 {
            return Err(Error::from_io_error(
                std::io::Error::last_os_error(),
                EINVAL,
            ));
        }

        let ret = unsafe { close(region.fd) };
        if ret < 0 {
            return Err(Error::from_io_error(
                std::io::Error::last_os_error(),
                EINVAL,
            ));
        }

        Ok(())
    }

    fn add_mem_region(&mut self, region: &MemoryRegion) -> Result<()>;
    fn del_mem_region(&mut self, region: &MemoryRegion) -> Result<()>;

    fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq>;
}

pub struct Blkio {
    driver: Box<dyn Driver>,
    allocated_regions: HashMap<MemoryRegion, bool>, // value is true iff region is registered
}

impl Blkio {
    pub fn new(driver: Box<dyn Driver>) -> Self {
        Blkio {
            driver,
            allocated_regions: HashMap::new(),
        }
    }

    pub fn connect(&mut self) -> Result<()> {
        self.driver.connect()
    }

    pub fn start(&mut self) -> Result<()> {
        self.driver.start()
    }

    pub fn get_bool(&self, name: &str) -> Result<bool> {
        self.driver.get_bool(name)
    }

    pub fn get_i32(&self, name: &str) -> Result<i32> {
        self.driver.get_i32(name)
    }

    pub fn get_str(&self, name: &str) -> Result<String> {
        self.driver.get_str(name)
    }

    pub fn get_u64(&self, name: &str) -> Result<u64> {
        self.driver.get_u64(name)
    }

    pub fn set_bool(&mut self, name: &str, value: bool) -> Result<()> {
        self.driver.set_bool(name, value)
    }

    pub fn set_i32(&mut self, name: &str, value: i32) -> Result<()> {
        self.driver.set_i32(name, value)
    }

    pub fn set_str(&mut self, name: &str, value: &str) -> Result<()> {
        self.driver.set_str(name, value)
    }

    pub fn set_u64(&mut self, name: &str, value: u64) -> Result<()> {
        self.driver.set_u64(name, value)
    }

    /// The allocated region is _not_ added (registered) by this method.
    pub fn alloc_mem_region(&mut self, len: usize) -> Result<MemoryRegion> {
        let region = self.driver.alloc_mem_region(len)?;
        self.allocated_regions.insert(region, false);
        Ok(region)
    }

    /// The given region must _not_ be registered when this method is called.
    pub fn free_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        self.allocated_regions.remove(region);
        self.driver.free_mem_region(region)
    }

    pub fn add_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        let align = self.get_u64("mem-region-alignment")? as usize;

        if region.addr % align != 0 {
            return Err(Error::new(
                EINVAL,
                format!(
                    "addr {:#x} violates mem-region-alignment {}",
                    region.addr, align
                ),
            ));
        }

        if region.len % align != 0 {
            return Err(Error::new(
                EINVAL,
                format!(
                    "len {:#x} violates mem-region-alignment {}",
                    region.len, align
                ),
            ));
        }

        self.driver.add_mem_region(region)?;

        self.allocated_regions
            .entry(*region)
            .and_modify(|s| *s = true);

        Ok(())
    }

    pub fn del_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        self.allocated_regions
            .entry(*region)
            .and_modify(|s| *s = false);

        self.driver.del_mem_region(region)
    }

    pub fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq> {
        self.driver.get_queue(index)
    }
}

impl Drop for Blkio {
    fn drop(&mut self) {
        for (region, &is_registered) in &self.allocated_regions {
            if is_registered {
                let _ = self.driver.del_mem_region(region);
            }

            let _ = self.driver.free_mem_region(region);
        }
    }
}
