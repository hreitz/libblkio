use crate::blkio::{
    self, Blkioq, Completion, Driver, Error, MemoryRegion, ReqFlags, Request, Result, State,
    ValidatedRequest,
};
use crate::properties;
use crate::properties::{PropertiesList, Property};
use libc::{fcntl, sigset_t, EBUSY, EINVAL, EIO, ENOTSUP, EROFS, F_GETFL, F_SETFL, O_NONBLOCK};
use std::cell::{RefCell, RefMut};
use std::os::unix::io::{AsRawFd, RawFd};
use std::rc::Rc;
use std::time::Duration;
use virtio_driver::{
    VhostUser, VirtioBlkConfig, VirtioBlkFeatureFlags, VirtioBlkQueue, VirtioFeatureFlags,
    VirtioTransport,
};
use vmm_sys_util::eventfd::EventFd;

pub const VHOST_USER_DRIVER: &str = "vhost_user";

// This is the maximum as defined in the virtio spec
const MAX_QUEUE_SIZE: i32 = 32768;
const DEFAULT_QUEUE_SIZE: i32 = 256;

pub struct Queue<'a> {
    vq: VirtioBlkQueue<'a>,
    submission_fd: Rc<EventFd>,
    completion_fd: Rc<EventFd>,
}

impl<'a> Queue<'a> {
    pub fn new(
        vq: VirtioBlkQueue<'a>,
        submission_fd: Rc<EventFd>,
        completion_fd: Rc<EventFd>,
    ) -> Self {
        Queue {
            vq,
            submission_fd,
            completion_fd,
        }
    }
}

fn validate_lba(offset: u64) -> Result<()> {
    // Adapt std::io::Error to blkio::Error
    virtio_driver::validate_lba(offset).map_err(|err| Error::from_io_error(err, EIO))
}

impl blkio::Queue for Queue<'_> {
    fn get_completion_fd(&self) -> RawFd {
        self.completion_fd.as_raw_fd()
    }

    fn set_completion_fd_enabled(&mut self, _enabled: bool) {
        // TODO: The transport could possibly disable the completion fd
    }

    fn validate(&self, req: Request) -> Result<ValidatedRequest> {
        match req {
            Request::Read {
                start,
                buf: _,
                len: _,
                user_data: _,
                flags,
            } => {
                validate_lba(start)?;
                blkio::validate_req_flags(flags, ReqFlags::empty())?
            }
            Request::Write {
                start,
                buf: _,
                len: _,
                user_data: _,
                flags,
            } => {
                validate_lba(start)?;
                blkio::validate_req_flags(flags, ReqFlags::empty())?
            }
            Request::Readv {
                start,
                iovec: _,
                iovcnt: _,
                user_data: _,
                flags,
            } => {
                validate_lba(start)?;
                blkio::validate_req_flags(flags, ReqFlags::empty())?
            }
            Request::Writev {
                start,
                iovec: _,
                iovcnt: _,
                user_data: _,
                flags,
            } => {
                validate_lba(start)?;
                blkio::validate_req_flags(flags, ReqFlags::empty())?
            }
            Request::WriteZeroes {
                start,
                len,
                user_data: _,
                flags,
            } => {
                validate_lba(start)?;
                validate_lba(start + len)?;
                blkio::validate_req_flags(flags, ReqFlags::NO_UNMAP | ReqFlags::NO_FALLBACK)?
            }
            Request::Discard {
                start,
                len,
                user_data: _,
                flags,
            } => {
                validate_lba(start)?;
                validate_lba(start + len)?;
                blkio::validate_req_flags(flags, ReqFlags::empty())?
            }
            Request::Flush {
                user_data: _,
                flags,
            } => blkio::validate_req_flags(flags, ReqFlags::empty())?,
        }

        Ok(ValidatedRequest(req))
    }

    fn try_enqueue(&mut self, req: &ValidatedRequest) -> bool {
        let result = match req.0 {
            Request::Read {
                start,
                buf,
                len,
                user_data,
                flags: _,
            } => unsafe { self.vq.read_raw(start, buf, len, user_data) },
            Request::Write {
                start,
                buf,
                len,
                user_data,
                flags: _,
            } => unsafe { self.vq.write_raw(start, buf, len, user_data) },
            Request::Readv {
                start,
                iovec,
                iovcnt,
                user_data,
                flags: _,
            } => unsafe { self.vq.readv(start, iovec, iovcnt as usize, user_data) },
            Request::Writev {
                start,
                iovec,
                iovcnt,
                user_data,
                flags: _,
            } => unsafe { self.vq.writev(start, iovec, iovcnt as usize, user_data) },
            Request::WriteZeroes {
                start,
                len,
                user_data,
                flags,
            } => {
                let unmap = !flags.contains(ReqFlags::NO_UNMAP);

                self.vq.write_zeroes(start, len, unmap, user_data)
            }
            Request::Discard {
                start,
                len,
                user_data,
                flags: _,
            } => self.vq.discard(start, len, user_data),
            Request::Flush {
                user_data,
                flags: _,
            } => self.vq.flush(user_data),
        };

        result.is_ok()
    }

    fn submit_and_wait(
        &mut self,
        min_completions: u32,
        timeout: Option<Duration>,
        sig: Option<&sigset_t>,
    ) -> Result<()> {
        if min_completions != 0 || timeout.is_some() || sig.is_some() {
            return Err(Error::new(ENOTSUP, "Not implemented"));
        }
        self.submission_fd
            .write(1)
            .map_err(|e| Error::from_io_error(e, EIO))
    }

    fn completions(&mut self) -> Box<dyn Iterator<Item = Completion> + '_> {
        Box::new(
            self.vq
                .completions()
                .map(|virtio_driver::Completion { user_data, ret }| Completion { user_data, ret }),
        )
    }
}

properties! {
    VIRTIO_BLK_PROPS: PropertyState for VirtioBlk.props {
        buf_alignment: i32,
        fn capacity: u64,
        discard_alignment: i32,
        discard_alignment_offset: i32,
        driver: str,
        //mut fd: i32,
        max_discard_len: u64,
        fn max_queues: i32,
        max_queue_size: i32,
        fn max_mem_regions: u64,
        max_segments: i32,
        max_transfer: i32,
        max_write_zeroes_len: u64,
        mem_region_alignment: u64,
        needs_mem_regions: bool,
        needs_mem_region_fd: bool,
        mut num_queues: i32,
        optimal_buf_alignment: i32,
        optimal_io_alignment: i32,
        optimal_io_size: i32,
        mut path: str,
        mut queue_size: i32,
        mut read_only: bool,
        request_alignment: i32
    }
}

pub struct VirtioBlk {
    state: State,
    props: PropertyState,
    queues: Vec<Blkioq>,
    transport: Option<RefCell<virtio_driver::VhostUser>>,
}

impl VirtioBlk {
    pub fn new() -> Self {
        VirtioBlk {
            props: PropertyState {
                buf_alignment: 1,
                discard_alignment: 512,
                discard_alignment_offset: 0,
                driver: VHOST_USER_DRIVER.to_string(),
                //fd: -1,
                max_discard_len: 0,
                max_queue_size: MAX_QUEUE_SIZE,
                max_segments: MAX_QUEUE_SIZE as i32,
                max_transfer: 0,
                max_write_zeroes_len: 0,
                mem_region_alignment: 1,
                needs_mem_regions: true,
                needs_mem_region_fd: true,
                num_queues: 1,
                optimal_buf_alignment: 1,
                optimal_io_alignment: 512,
                optimal_io_size: 0,
                queue_size: DEFAULT_QUEUE_SIZE,
                path: String::new(),
                read_only: false,
                request_alignment: 512,
            },
            queues: Vec::new(),
            state: State::Created,
            transport: None,
        }
    }

    // FIXME Share this code with io_uring
    fn cant_set_while_connected(&self) -> Result<()> {
        if self.state >= State::Connected {
            Err(properties::error_cant_set_while_connected())
        } else {
            Ok(())
        }
    }

    fn cant_set_while_started(&self) -> Result<()> {
        if self.state >= State::Started {
            Err(properties::error_cant_set_while_started())
        } else {
            Ok(())
        }
    }

    fn must_be_connected(&self) -> Result<()> {
        if self.state >= State::Connected {
            Ok(())
        } else {
            Err(properties::error_must_be_connected())
        }
    }

    fn must_be_started(&self) -> Result<()> {
        if self.state >= State::Started {
            Ok(())
        } else {
            Err(Error::new(EBUSY, "Device must be started"))
        }
    }

    fn get_transport(&self) -> RefMut<'_, VhostUser> {
        self.transport.as_ref().unwrap().borrow_mut()
    }

    fn get_capacity(&self) -> Result<u64> {
        self.must_be_connected()?;

        let cfg = self
            .get_transport()
            .get_config::<VirtioBlkConfig>()
            .map_err(|e| Error::from_io_error(e, EIO))?;
        Ok(512 * u64::from(cfg.capacity))
    }

    fn get_max_queues(&self) -> Result<i32> {
        self.must_be_connected()?;

        Ok(self.get_transport().max_queues() as i32)
    }

    fn get_max_mem_regions(&self) -> Result<u64> {
        self.must_be_connected()?;

        Ok(self.get_transport().max_mem_regions())
    }

    fn set_queue_size(&mut self, value: i32) -> Result<()> {
        self.must_be_connected()?;
        self.cant_set_while_started()?;

        if value <= 0 {
            return Err(Error::new(EINVAL, "queue_size must be greater than 0"));
        }
        if !(value as u32).is_power_of_two() {
            return Err(Error::new(EINVAL, "queue_size must be a power of two"));
        }
        if value > MAX_QUEUE_SIZE {
            return Err(Error::new(
                EINVAL,
                format!("queue_size must be smaller than {}", MAX_QUEUE_SIZE),
            ));
        }

        self.props.queue_size = value;
        Ok(())
    }

    fn set_num_queues(&mut self, value: i32) -> Result<()> {
        self.must_be_connected()?;
        self.cant_set_while_started()?;

        if value <= 0 {
            return Err(Error::new(EINVAL, "num_queues must be greater than 0"));
        }

        let max_queues = self.get_max_queues()?;
        if value > max_queues {
            return Err(Error::new(
                EINVAL,
                format!("num_queues must not be greater than {}", max_queues),
            ));
        }

        self.props.num_queues = value;
        Ok(())
    }

    fn set_path(&mut self, value: &str) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.path = value.to_string();
        Ok(())
    }

    fn set_read_only(&mut self, value: bool) -> Result<()> {
        self.cant_set_while_connected()?;
        self.props.read_only = value;
        Ok(())
    }
}

impl Driver for VirtioBlk {
    fn state(&self) -> State {
        self.state
    }

    fn connect(&mut self) -> Result<()> {
        self.cant_set_while_started()?;

        if self.state == State::Connected {
            return Ok(());
        }

        if self.props.path.is_empty() {
            return Err(Error::new(EINVAL, "path must be set"));
        }

        let features = VirtioBlkFeatureFlags::all().bits() | VirtioFeatureFlags::VERSION_1.bits();
        let mut vhost = VhostUser::new(&self.props.path, features)
            .map_err(|_e| Error::new(EIO, "Failed to connect to vhost socket"))?;

        let features = VirtioBlkFeatureFlags::from_bits_truncate(vhost.get_features());
        let cfg = vhost
            .get_config::<VirtioBlkConfig>()
            .map_err(|e| Error::from_io_error(e, EIO))?;

        if !features.contains(VirtioBlkFeatureFlags::DISCARD) {
            return Err(Error::new(EINVAL, "Device does not support discard"));
        }
        if !features.contains(VirtioBlkFeatureFlags::WRITE_ZEROES) {
            return Err(Error::new(EINVAL, "Device does not support write_zeroes"));
        }
        self.props.discard_alignment = 512 * cfg.discard_sector_alignment.to_native() as i32;
        self.props.max_discard_len = 512 * cfg.max_discard_sectors.to_native() as u64;
        self.props.max_write_zeroes_len = 512 * cfg.max_write_zeroes_sectors.to_native() as u64;

        let blk_size = if features.contains(VirtioBlkFeatureFlags::BLK_SIZE) {
            cfg.blk_size.to_native() as i32
        } else {
            512
        };

        self.props.request_alignment = blk_size;
        self.props.optimal_io_alignment = blk_size;

        if features.contains(VirtioBlkFeatureFlags::TOPOLOGY) {
            self.props.optimal_io_alignment = blk_size * 2i32.pow(cfg.physical_block_exp as u32);
            self.props.optimal_io_size = blk_size * cfg.opt_io_size.to_native() as i32;
            self.props.discard_alignment_offset = blk_size * cfg.alignment_offset as i32;
        }

        self.transport = Some(RefCell::new(vhost));
        self.state = State::Connected;

        Ok(())
    }

    fn start(&mut self) -> Result<()> {
        self.must_be_connected()?;

        if self.state == State::Started {
            return Ok(());
        }

        let mut transport = self.transport.as_ref().unwrap().borrow_mut();
        let features = VirtioBlkFeatureFlags::from_bits_truncate(transport.get_features());

        if features.contains(VirtioBlkFeatureFlags::RO) && !self.props.read_only {
            return Err(Error::new(EROFS, "Device is read-only"));
        }

        let queues = VirtioBlkQueue::setup_queues(
            &mut *transport,
            self.props.num_queues as usize,
            self.props.queue_size as u16,
        );

        self.queues = queues
            .map_err(|e| Error::from_io_error(e, EIO))?
            .into_iter()
            .enumerate()
            .map(|(i, q)| {
                let submission_fd = transport.get_submission_fd(i);
                let completion_fd = transport.get_completion_fd(i);

                let completion_status_flags = unsafe { fcntl(completion_fd.as_raw_fd(), F_GETFL) };
                if completion_status_flags < 0 {
                    return Err(Error::from_last_os_error());
                }

                if unsafe {
                    fcntl(
                        completion_fd.as_raw_fd(),
                        F_SETFL,
                        completion_status_flags | O_NONBLOCK,
                    )
                } != 0
                {
                    return Err(Error::from_last_os_error());
                }

                Ok(Blkioq::new(Box::new(Queue::new(
                    q,
                    Rc::clone(&submission_fd),
                    Rc::clone(&completion_fd),
                ))))
            })
            .collect::<Result<_>>()?;

        self.state = State::Started;
        Ok(())
    }

    fn add_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        self.must_be_started()?;
        self.get_transport()
            .add_mem_region(region.addr, region.len, region.fd, region.fd_offset)
            .map_err(|e| Error::from_io_error(e, EIO))
    }

    fn del_mem_region(&mut self, region: &MemoryRegion) -> Result<()> {
        self.must_be_started()?;
        self.get_transport()
            .del_mem_region(region.addr, region.len)
            .map_err(|e| Error::from_io_error(e, EIO))
    }

    fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq> {
        self.queues
            .get_mut(index)
            .ok_or_else(|| Error::new(EINVAL, "invalid queue index"))
    }
}
