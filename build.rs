fn main() {
    cc::Build::new()
        .flag("-std=c11")
        .opt_level(2)
        .file("src/error-msg.c")
        .compile("error-msg");
}
